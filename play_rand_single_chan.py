import mido
from mido import MidiFile
import os, random
from music21.chord import Chord
from music21.pitch import Pitch
import sys
import instrument
# get random file
midi_folder = "C:\\Users\\Sven\\Desktop\\130000midi\\"
midi_files  = [os.path.join(path, filename)
         for path, dirs, files in os.walk(midi_folder)
         for filename in files
         if filename.endswith(".mid")]
random_number = random.randint(0, len(midi_files)-1)
# random_number = 710
# random_number = 26340
# random_number = 9204 triad hell
random_midifile = midi_files[random_number]
print('num: ' + str(random_number))
print('file: ' + str(random_midifile))

# get correct output
output_name = None
for name in  mido.get_output_names():
	if name in ['Saffire', 'Yamaha', 'USB2.0-MIDI']:
		output_name = name

volume_scale = 0.5

with mido.open_output(name=output_name) as output:
    try:
        with MidiFile(random_midifile) as midi_file:
            chord = []
            melody = []
            for message in midi_file.play():
            	if hasattr(message,'velocity'): message.velocity = int(message.velocity*volume_scale)
                # print message.channel
                output.send(message)
                if message.type!="note_on" and message.type!="note_off":
                    if message.type=="program_change":
                        print message.channel, message.program, instrument.category[message.program].ljust(20), " - ",instrument.name[message.program]
                # chord detection
                if message.type=="note_on" and message.channel!=9:
                    p = Pitch(message.note)
                    if message.time==0:
                        chord.append(p)
                    else:
                        if len(chord)>0:
                                if len(chord)>2:
                                    c = Chord(list(set([int(note.ps) for note in chord])))
                                    sys.stdout.write(("\nChord: " + " ".join([note.nameWithOctave for note in chord])).ljust(40) + ", " + c.pitchedCommonName + "... ")
                                else: 
                                    sys.stdout.write(" ".join([note.nameWithOctave for note in chord]))
                                chord = []
                        else:
                            sys.stdout.write(str(p.nameWithOctave) + "," + str(message.channel) + " ")
                            melody.append(message)
                    sys.stdout.flush()
    except KeyboardInterrupt:
        output.reset()
print('\nnum: ' + str(random_number))
print('file: ' + str(random_midifile))

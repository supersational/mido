import mido
from datetime import datetime

# get correct output
output_name = None
for name in  mido.get_output_names():
	print name
	if name in ['Saffire', 'Yamaha', 'USB2.0-MIDI']:
		output_name = name

with mido.open_output(name=output_name) as output:

    print(output)
    msg = mido.Message('sysex', data=[67,115,127,0,27,4])
    msg = mido.Message('sysex', data=[67,115,127,0,27,127])
    output.send(msg)
    output.send(mido.Message('note_on', note=64))

import re
text = """
====Piano====
*1   [[Grand piano|Acoustic Grand Piano]]
*2   [[Piano|Bright Acoustic Piano]]
*3  [[Electric piano|Electric Grand Piano]]
*4  [[Tack piano|Honky-tonk Piano]]
*5  [[Rhodes piano|Electric Piano]] 1
*6   Electric Piano 2
*7   [[Harpsichord]]
*8   [[Clavinet]]

====Chromatic Percussion====
*9   [[Celesta]]
*10   [[Glockenspiel]]
*11  [[Musical box|Music Box]]
*12  [[Vibraphone]]
*13  [[Marimba]]
*14  [[Xylophone]]
*15  [[Tubular bell|Tubular Bells]]
*16  [[Hammered dulcimer|Dulcimer]]

====Organ====
*17  [[Hammond organ|Drawbar Organ]]
*18  [[Hammond organ#Harmonic Percussion|Percussive Organ]]
*19  [[Hammond organ|Rock Organ]]
*20  [[Church Organ]]
*21  [[Reed Organ]]
*22  [[Accordion]]
*23  [[Harmonica]]
*24  [[Bandoneon|Tango Accordion]]

====Guitar====
*25  [[Classical guitar|Acoustic Guitar (nylon)]]
*26  [[Steel-string acoustic guitar|Acoustic Guitar (steel)]]
*27  [[Jazz guitar|Electric Guitar (jazz)]]
*28  [[Electric guitar|Electric Guitar (clean)]]
*29  [[Electric guitar|Electric Guitar (muted)]]
*30  [[Electric guitar|Overdriven Guitar]]
*31  [[Electric guitar|Distortion Guitar]]
*32  [[Harmonic|Guitar Harmonics]]

====Bass====
*33  [[Acoustic bass guitar|Acoustic Bass]]
*34  [[Bass guitar|Electric Bass (finger)]]
*35  [[Bass guitar|Electric Bass (pick)]]
*36  [[Fretless bass guitar#Fretted and fretless basses|Fretless Bass]]
*37  [[Slapping (music)|Slap Bass 1]]
*38  Slap Bass 2
*39  [[Synthesizer#Synth bass|Synth Bass 1]]
*40  Synth Bass 2

====Strings====
*41  [[Violin]]
*42  [[Viola]]
*43  [[Cello]]
*44  [[Contrabass]]
*45  [[Tremolo]] Strings
*46  [[Pizzicato]] Strings
*47  [[Harp|Orchestral Harp]]
*48  [[Timpani]]

====Ensemble====
*49  [[String section|String Ensemble 1]]
*50  String Ensemble 2
*51  [[Synthesizer|Synth]] [[String instrument|Strings]] 1
*52  Synth Strings 2
*53  [[Choir]] Aahs
*54  [[Human voice|Voice]] Oohs
*55  Synth Choir
*56  [[Orchestra hit|Orchestra Hit]]

====Brass====
*57  [[Trumpet]]
*58  [[Trombone]]
*59  [[Tuba]]
*60  [[Muted trumpet|Muted Trumpet]]
*61  [[French Horn]]
*62  [[Brass Section]]
*63  Synth Brass 1
*64  Synth Brass 2

====Reed====
*65  [[Soprano saxophone|Soprano Sax]]
*66  [[Alto saxophone|Alto Sax]]
*67  [[Tenor saxophone|Tenor Sax]]
*68  [[Baritone saxophone|Baritone Sax]]
*69  [[Oboe]]
*70  [[Cor anglais|English Horn]]
*71  [[Bassoon]]
*72  [[Clarinet]]

====Pipe====
*73  [[Piccolo]]
*74  [[Flute]]
*75  [[Recorder (musical instrument)|Recorder]]
*76  [[Pan Flute]]
*77  [[Blown bottle]]
*78  [[Shakuhachi]]
*79  [[Whistle]]
*80  [[Ocarina]]

====Synth Lead====
*81  [[Synthesizer#Synth lead|Lead 1]] ([[square wave|square]])
*82  Lead 2 ([[sawtooth wave|sawtooth]])
*83  Lead 3 ([[Calliope (music)|calliope]])
*84  Lead 4 chiff
*85  Lead 5 ([[charang]])
*86  Lead 6 (voice)
*87  Lead 7 ([[Perfect fifth|fifths]])
*88  Lead 8 (bass + lead)

====Synth Pad====
*89  Pad 1 (new age)
*90  Pad 2 (warm)
*91  Pad 3 ([[synthesizer|polysynth]])
*92  Pad 4 (choir)
*93  Pad 5 (bowed)
*94  Pad 6 (metallic)
*95  Pad 7 (halo)
*96  Pad 8 (sweep)

====Synth Effects====
*97  [[Sound effect|FX]] 1 ([[rain]])
*98  FX 2 (soundtrack)
*99  FX 3 (crystal)
*100  FX 4 (atmosphere)
*101 FX 5 (brightness)
*102 FX 6 ([[goblin]]s)
*103 FX 7 ([[echo (phenomenon)|echo]]es)
*104 FX 8 (sci-fi)

====Ethnic====
*105 [[Sitar]]
*106 [[Banjo]]
*107 [[Shamisen]]
*108 [[Koto (musical instrument)|Koto]]
*109 [[Marimbula|Kalimba]]
*110 [[Bagpipes|Bagpipe]]
*111 [[Fiddle]]
*112 [[Shehnai|Shanai]]

====Percussive====
*113 Tinkle Bell
*114 [[Agogo|Agogo]]
*115 [[Steelpan|Steel Drums]]
*116 [[Wood block|Woodblock]]
*117 [[Taiko|Taiko Drum]]
*118 [[Tom-tom drum|Melodic Tom]]
*119 Synth Drum
*120 Reverse [[Cymbal]]

====Sound effects====
*121 Guitar [[Fret]] Noise
*122 Breath Noise
*123 [[Coast|Seashore]]
*124 [[Bird vocalisation|Bird Tweet]]
*125 [[Ring (telephone)|Telephone Ring]]
*126 [[Helicopter]]
*127 [[Applause]]
*128 [[Gunshot]]
"""

instrument = ""
d = {}
l = {}
category = {}
# inst = namedtuple('Instrument', [])
for line in text.split("\n"):
    if len(line)>4:
        if line[0:4]=="====":
            print line
            instrument = re.findall('=+([\s\w]+)=+', line)[0]
            d[instrument] = []
        elif line[0]=="*":
            print line
            name = re.findall('\*[0-9]+\s+\[*([\w\s]+)', line)[0]
            num = re.findall('\*([0-9]+)', line)[0]
            d[instrument].append(name)
            l[int(num)-1] = name
            category[int(num)-1] = instrument

print d
print l
with open("instrument.py",'w') as f:
    f.write("name=" + str(l) + "\ncategory="+str(category))
    
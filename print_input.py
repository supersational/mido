import mido
from datetime import datetime

# get correct input
input_name = None
for name in  mido.get_input_names():
	print name
	if name in ['Saffire', 'Yamaha', 'USB2.0-MIDI']:
		input_name = name

with mido.open_input(name=input_name) as input:
    print(input)
    while 1:
        for message in input:
        	if message.type != "clock": print datetime.now().strftime("%M:%S.%f"), message


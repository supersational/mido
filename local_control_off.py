import mido
from datetime import datetime

# get correct output
output_name = None
for name in  mido.get_output_names():
	print name
	if name in ['Saffire', 'Yamaha', 'USB2.0-MIDI']:
		output_name = name

with mido.open_output(name=output_name) as output:
    msg = mido.parse([176,122,0])
    print msg
    output.send(msg)
import mido
from mido import MidiFile
import os, random, sys
from music21 import *
us = environment.UserSettings()
us['lilypondPath'] = U"""C:\\Users\\Sven\\Desktop\\programming\\LilyPond\\usr\\bin\\lilypond.exe"""

midi_folder = "C:\\Users\\Sven\\Desktop\\130000midi\\"
print( "getting file from " + midi_folder)
midi_files  = [os.path.join(path, filename)
         for path, dirs, files in os.walk(midi_folder)
         for filename in files
         if filename.endswith(".mid")]
random_midifile = random.choice(midi_files)
random_midifile = midi_files[100]
print( random_midifile)

# get correct output
output_name = None
for name in  mido.get_output_names():
	if name in ['Saffire', 'Yamaha', 'USB2.0-MIDI']:
		output_name = name

if 0:
    num = 20
    with MidiFile(random_midifile) as midi_file:
        for message in midi_file:
            print( num, message)
            num = num -  1
            if num<0: break
# print( "parsing..")
# s = converter.parseFile(random_midifile)
# print( "showing..")
# s.show("lilypond")

# with mido.open_output(name=output_name) as output:
#     print((output))
#     with MidiFile(random_midifile) as midi_file:
#         for message in midi_file.play():
#         	if hasattr(message,'velocity'): message.velocity = int(message.velocity*volume_scale)
#             print((message))
#             output.send(message)

print( "mf = midi.MidiFile()")
mf = midi.MidiFile()
print( "mf.open(random_midifile)")
mf.open(random_midifile)
print( "mf.read()")
mf.read()
print( "mf.close()")
mf.close()
print( "s = midi.translate.midiFileToStream(mf)")
s = midi.translate.midiFileToStream(mf)
s.show()
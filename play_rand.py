# from __future__ import print_function
import mido
from mido import MidiFile, MetaMessage
import os, random, time
from music21.chord import Chord
from music21.pitch import Pitch
import sys, math
import instrument

print "started"
midi_folder = "C:\\Users\\Sven\\Desktop\\800000_Drum\\800000_Drum_Percussion_MIDI_Archive[6_19_15]\\"
midi_folder = "C:\\Users\\Sven\\Desktop\\130000midi\\"
midi_file_list = os.path.join(os.path.dirname(os.path.abspath(__file__)),"dump_" + midi_folder.split("\\")[-2])+".txt"
print midi_file_list
if os.path.isfile(midi_file_list):
    print "getting file from previous dump.. (it takes too long to os.walk all over the place)"
    random_midifile = str.strip(random.choice(list(open(midi_file_list))))
    random_number = -1
else:
    # get random file
    midi_files  = [os.path.join(path, filename)
             for path, dirs, files in os.walk(midi_folder)
             for filename in files
             if filename.endswith(".mid")]

    with open(midi_file_list, "w+") as f:
        for item in midi_files:
          f.write("%s\n" % item)
    random_number = random.randint(0, len(midi_files)-1)
    # random_number = 710
    # random_number = 26340
    # random_number = 9204 #triad hell
    # random_number = 7376 #smooooth triads
    # random_number = 19180 #smooooth triads
    # random_number = 19761
    # random_number = 37855
    random_midifile = midi_files[random_number]
    print 'num: ' + str(random_number)
print 'file: ' + str(random_midifile)
print 'file: ' + str(random_midifile.split("\\")[-1])

# get correct output
output_name = None
print mido.get_output_names()
for name in  mido.get_output_names():
	if any(map(lambda x: x in name, ['Saffire(disable)', 'Yamaha', 'USB2.0-MIDI'])):
		output_name = name
print "opening %s midi device..." % output_name
with mido.open_output(name=output_name) as output:
    try:
        volume_scale = 0.5
        with MidiFile(random_midifile) as midi_file:
            chords = [[] for chan in range(16)]
            melody = []
            instruments = ["default" for chan in range(16)]
            t = 0
            t_done = False
            tempo = 500000 
            for message in midi_file.play(meta_messages=True):
                t += message.time
                s_per_beat = tempo / 1000000.0
                diff = ((t - message.time) % s_per_beat) - (t % s_per_beat)
                while diff > 0 :
                    diff -= s_per_beat
                    if len(melody) > 0:
                        melody = []
                    sys.stdout.write(".")
                    sys.stdout.flush()
                    t_done = True

                if isinstance(message, MetaMessage):
                    if len(melody)>0:
                        sys.stdout.write("\n")

                    if message.type=="set_tempo":
                        print "tempo:" + str(message.tempo)
                        tempo = message.tempo
                    elif message.type=="lyrics":
                        print "lyrics:" + message.text
                    elif message.type=="text":
                        print "text: " + message.text
                    elif message.type=="program_change":
                        print str(message.channel) + " " + str(message.program)+ instrument.category[message.program].ljust(20)+" - "+instrument.name[message.program]
                        instruments[message.channel] = instrument.name[message.program]
                    elif message.type=="pitchwheel":
                        sys.stdout.write(message)
                    else:
                        print message
                    continue

                
                message.channel = 9 # set to drum channel

                if hasattr(message,'velocity'): message.velocity = int(message.velocity*volume_scale)
                # print message.channel
                output.send(message)
                # chord detection
                if message.type=="note_on" and message.channel!=9:
                    p = Pitch(message.note)
                    # sys.stdout.write(str(p.nameWithOctave).ljust(4) + " " + instruments[message.channel])
                    if t_done:
                        t_done = False
                        sys.stdout.write("\n")                           
                    sys.stdout.write(str(p.nameWithOctave)+ " ") 
                    sys.stdout.flush()
                    if message.time==0:
                        chords[message.channel].append(p)
                    else:
                        for chord in chords:
                            if len(chord)>0:
                                if len(chord)>1:
                                    c = Chord(list(set([int(note.ps) for note in chord])))
                                    notes = [str(note.nameWithOctave)  for note in sorted(set(chord), key=lambda x: x.ps)]
                                    print ("\n" + str(message.channel) + " " + "Chord: " + " ".join(notes)).ljust(40) + ", " + c.pitchedCommonName + "... "
                        chords = [[] for chan in range(16)]
                            # else:
                    melody.append(message)
                elif message.channel==9:
                    print instrument.drum_note(message.note)
    except KeyboardInterrupt:
        output.reset()
print 'file: ' + str(random_midifile)
print '\nnum: ' + str(random_number)
